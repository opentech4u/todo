const   express =   require('express');

const   router  =   express.Router();

const   todoController  =   require('../controllers/todoController');

router
.route('/')
.get(todoController.todoDashboard);

//router.post('/todo',todoController.todoAdditem);

module.exports = router;