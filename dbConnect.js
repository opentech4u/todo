const mysql =   require('mysql');

const conn  =   mysql.createPool({
    connectionLimit :   10,
    host            :   'localhost',
    user            :   'root',
    password        :   'teachers',
    database        :   'todo'
});

conn.getConnection((err,connection)=>{
    if(err)
        console.log("Falied To Connect!");
    if(connection)
        connection.release();
    return;
});

module.exports  =   conn;