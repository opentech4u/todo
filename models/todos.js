const conn = require('../dbConnect');

const get_details = (callback) => {
    let sql = "select * from todo_list where comp_flag = 'P'";
    conn.query(sql, (err, rows) => {
        if (err) {
            throw err;
        } else {
            callback(rows);
        }
    });
}

module.exports = {
    get_details
}