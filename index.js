const express =   require('express');

const app =   express();

const todoRouter = require('./routes/todoRouter');

//set up template engine
app.set('view engine','ejs');

//static files
app.use(express.static(__dirname + '/public'));

app.use('/',todoRouter);

//listen to port
app.listen(3000);
console.log('you are listening to port 3000');